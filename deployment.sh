#!/bin/bash



Micro_service_name=$1




# Below block related to Shop-UI micro-service start-up script

if [ "$Micro_service_name" == "Shop-UI" ]; then



      if [[ "$(docker ps -q -f name=Shop-UI)" ]]; then

           echo "stoping the SHOP-UI container"
           docker stop Shop-UI
           echo "SHOP-UI container stopped"
      fi




      if [[ "$(docker ps -a -q -f name=Shop-UI)" ]]; then

           echo "removing the SHOP-UI container"
           docker rm Shop-UI
           echo "SHOP-UI container removed"
      fi




      export shop_ui_image="$(cat shop-ui-image-name.txt)"

      echo "Loading new shop-ui image name $shop_ui_image"

      echo "Starting the shop-ui container"

      export product_catalogue_image="$(cat product-catalogue-image-name.txt)"

      docker-compose up -d Shop-UI






#below block related to Product-catalogue micro-service start-up script


elif [ "$Micro_service_name" == "Product-Catalogue" ]; then




    if [[ "$(docker ps -q -f name=productcatalogue)" ]]; then

    echo "stoping the product-catalogue  container"
    docker stop productcatalogue
    echo "product-catalogue  container stopped"
    fi


    if [[ "$(docker ps -a -q -f name=productcatalogue)" ]]; then

    echo "removing the product-catalogue container"
    docker rm productcatalogue
    echo "product-catalogue container removed"
    fi




    export product_catalogue_image="$(cat product-catalogue-image-name.txt)"

    echo "Loading new product-catalogue  image name $product_catalogue_image"

    echo "Starting the product-catalogue  container"

    export shop_ui_image="$(cat shop-ui-image-name.txt)"

    docker-compose up -d Product-Catalogue




#below block related to stock-manager micro-service startup script  this block is not ready need to edit

elif [ "$Micro_service_name)" == "Stock-Manager" ]; then



    if [[ "$(docker ps -q -f name=Stock-Manager)" ]]; then

        echo "stoping the Stock-Manager container"
        docker stop Stock-Manager
        echo "Stock-Manager container stopped"
    fi



    if [[ "$(docker ps -a -q -f name=Stock-Manager)" ]]; then

        echo "removing the Stock-Manager container"
        docker rm Stock-Manager
        echo "SHOP-UI container removed"
    fi




    export shop_ui_image="$(cat shop-ui-image-name.txt)"

    echo "Loading new shop-ui image name $shop_ui_image"

    echo "Starting the shop-ui container"

    docker-compose up -d


else 
        echo " No container on this name : "

fi
